"""REST client handling, including RecurlyStream base class."""

import requests
from pathlib import Path
from typing import Any, Dict, Optional
from base64 import b64encode
from memoization import cached

from singer_sdk.streams import RESTStream
from urllib.parse import urlparse
from urllib.parse import parse_qs
from singer_sdk.exceptions import FatalAPIError, RetriableAPIError
import time


class RecurlyStream(RESTStream):
    """Recurly stream class."""

    api_version = "v2021-02-25"
    _page_size = 1

    @property
    def url_base(self) -> str:
        """Return the API URL root, configurable via tap settings."""
        if self.config.get("region", "US").lower() == "us":
            return "https://v3.recurly.com"
        else:
            return "https://v3.eu.recurly.com"

    records_jsonpath = "$.data[*]"
    next_page_token_jsonpath = "$.next_page"

    @property
    def http_headers(self) -> dict:
        basic_auth = b64encode(bytes(self.config.get("api_key") + ":", "ascii")).decode(
            "ascii"
        )
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        headers["Authorization"] = "Basic %s" % basic_auth
        headers["Accept"] = f"application/vnd.recurly.{self.api_version}"
        headers["Content-Type"] = "application/json"
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        next_page_token = None
        res = response.json()
        if not res.get("has_more"):
            return None

        if res.get("next"):
            url = res.get("next")
            parsed_url = urlparse(url)
            parsed_params = parse_qs(parsed_url.query)
            if "cursor" in parsed_params:
                next_page_token = parsed_params["cursor"][0]
        return next_page_token

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        if next_page_token:
            params["cursor"] = next_page_token
        if self.replication_key:
            if self.get_starting_timestamp(context):
                params["begin_time"] = self.get_starting_timestamp(context).isoformat()
        params["limit"] = self._page_size
        # params['sort'] = "created_at"
        params["sort"] = "updated_at"
        return params
    
    def validate_response(self, response: requests.Response) -> None:
        response_headers = response.headers
        if "x-ratelimit-reset" and "x-ratelimit-remaining" in response_headers:
            limit_reset = int(response_headers['x-ratelimit-reset'])
            limit_remaining = int(response_headers['x-ratelimit-remaining'])
            if limit_remaining <= 10:
                current_timestamp_utc = int(time.time())
                seconds_difference = limit_reset - current_timestamp_utc
                if seconds_difference > 0:
                    time.sleep(seconds_difference)
        if (
            response.status_code in self.extra_retry_statuses
            or 500 <= response.status_code < 600
        ):
            self.logger.warn(f"Response headers: {response_headers}")
            msg = self.response_error_message(response)
            raise RetriableAPIError(msg, response)
        elif 400 <= response.status_code < 500:
            msg = self.response_error_message(response)
            raise FatalAPIError(msg)
