"""Stream type classes for tap-recurly."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th

from tap_recurly.client import RecurlyStream


class AccountsStream(RecurlyStream):
    name = "accounts"
    path = "/accounts"
    primary_keys = ["id"]
    replication_key = "updated_at"

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("object", th.StringType),
        th.Property("state", th.StringType),
        th.Property("hosted_login_token", th.StringType),
        th.Property(
            "shipping_addresses",
            th.ArrayType(
                th.ObjectType(
                    th.Property("id", th.StringType),
                    th.Property("object", th.StringType),
                    th.Property("account_id", th.StringType),
                    th.Property("nickname", th.StringType),
                    th.Property("first_name", th.StringType),
                    th.Property("last_name", th.StringType),
                    th.Property("company", th.StringType),
                    th.Property("email", th.StringType),
                    th.Property("vat_number", th.StringType),
                    th.Property("phone", th.StringType),
                    th.Property("street1", th.StringType),
                    th.Property("street2", th.StringType),
                    th.Property("city", th.StringType),
                    th.Property("region", th.StringType),
                    th.Property("postal_code", th.StringType),
                    th.Property("country", th.StringType),
                    th.Property("created_at", th.DateTimeType),
                    th.Property("updated_at", th.DateTimeType),
                )
            ),
        ),
        th.Property("has_live_subscription", th.BooleanType),
        th.Property("has_active_subscription", th.BooleanType),
        th.Property("has_future_subscription", th.BooleanType),
        th.Property("has_canceled_subscription", th.BooleanType),
        th.Property("has_paused_subscription", th.BooleanType),
        th.Property("has_past_due_invoice", th.BooleanType),
        th.Property("created_at", th.DateTimeType),
        th.Property("updated_at", th.DateTimeType),
        th.Property("deleted_at", th.DateTimeType),
        th.Property("code", th.StringType),
        th.Property("username", th.StringType),
        th.Property("username", th.StringType),
        th.Property("email", th.StringType),
        th.Property("preferred_locale", th.StringType),
        th.Property("preferred_time_zone", th.StringType),
        th.Property("cc_emails", th.StringType),
        th.Property("first_name", th.StringType),
        th.Property("last_name", th.StringType),
        th.Property("company", th.StringType),
        th.Property("vat_number", th.StringType),
        th.Property("tax_exempt", th.BooleanType),
        th.Property("exemption_certificate", th.StringType),
        th.Property("parent_account_id", th.StringType),
        th.Property("bill_to", th.StringType),
        th.Property("dunning_campaign_id", th.StringType),
        th.Property("invoice_template_id", th.StringType),
        th.Property(
            "address",
            th.ObjectType(
                th.Property("phone", th.StringType),
                th.Property("street1", th.StringType),
                th.Property("street2", th.StringType),
                th.Property("city", th.StringType),
                th.Property("region", th.StringType),
                th.Property("postal_code", th.StringType),
                th.Property("country", th.StringType),
            ),
        ),
        th.Property(
            "billing_info",
            th.ObjectType(
                th.Property("id", th.StringType),
                th.Property("object", th.StringType),
                th.Property("account_id", th.StringType),
                th.Property("first_name", th.StringType),
                th.Property("last_name", th.StringType),
                th.Property("company", th.StringType),
                th.Property("address", th.CustomType({"type": ["object", "string"]})),
                th.Property("vat_number", th.StringType),
                th.Property("valid", th.BooleanType),
                th.Property(
                    "payment_method", th.CustomType({"type": ["object", "string"]})
                ),
                th.Property("fraud", th.CustomType({"type": ["object", "string"]})),
                th.Property("primary_payment_method", th.BooleanType),
                th.Property("backup_payment_method", th.BooleanType),
                th.Property(
                    "updated_by", th.CustomType({"type": ["object", "string"]})
                ),
                th.Property("created_at", th.DateTimeType),
                th.Property("updated_at", th.DateTimeType),
            ),
        ),
        th.Property("custom_fields", th.CustomType({"type": ["array", "string"]})),
    ).to_dict()


class InvoicesStream(RecurlyStream):
    name = "invoices"
    path = "/invoices"
    primary_keys = ["id"]
    replication_key = "updated_at"

    schema = th.PropertiesList(
        th.Property("name", th.StringType),
        th.Property("account", th.CustomType({"type": ["object", "string"]})),
        th.Property("address", th.CustomType({"type": ["object", "string"]})),
        th.Property("balance", th.NumberType),
        th.Property("billing_info_id", th.StringType),
        th.Property("closed_at", th.DateTimeType),
        th.Property("collection_method", th.StringType),
        th.Property("created_at", th.DateTimeType),
        th.Property("credit_payments", th.CustomType({"type": ["array", "string"]})),
        th.Property("currency", th.StringType),
        th.Property("customer_notes", th.StringType),
        th.Property("discount", th.NumberType),
        th.Property("due_at", th.DateTimeType),
        th.Property("dunning_campaign_id", th.StringType),
        th.Property("dunning_events_sent", th.NumberType),
        th.Property("final_dunning_event", th.BooleanType),
        th.Property("has_more_line_items", th.BooleanType),
        th.Property("id", th.StringType),
        th.Property("line_items", th.ArrayType(th.CustomType({"type": ["object", "string"]}))),
        th.Property("net_terms", th.NumberType),
        th.Property("number", th.StringType),
        th.Property("object", th.StringType),
        th.Property("origin", th.StringType),
        th.Property("paid", th.NumberType),
        th.Property("po_number", th.StringType),
        th.Property("previous_invoice_id", th.StringType),
        th.Property("refundable_amount", th.NumberType),
        th.Property("shipping_address", th.CustomType({"type": ["object", "string"]})),
        th.Property("state", th.StringType),
        th.Property("subscription_ids", th.CustomType({"type": ["array", "string"]})),
        th.Property("subtotal", th.NumberType),
        th.Property("tax", th.NumberType),
        th.Property("tax_info", th.CustomType({"type": ["object", "string"]})),
        th.Property("terms_and_conditions", th.StringType),
        th.Property("total", th.NumberType),
        th.Property("transactions", th.CustomType({"type": ["array", "string"]})),
        th.Property("type", th.StringType),
        th.Property("updated_at", th.DateTimeType),
        th.Property("uuid", th.StringType),
        th.Property("vat_number", th.StringType),
        th.Property("vat_reverse_charge_notes", th.StringType),
    ).to_dict()


class SubscriptionsStream(RecurlyStream):
    name = "subscriptions"
    path = "/subscriptions"
    primary_keys = ["id"]
    replication_key = "updated_at"

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("object", th.StringType),
        th.Property("uuid", th.StringType),
        th.Property(
            "account",
            th.ObjectType(
                th.Property("id", th.StringType),
                th.Property("object", th.StringType),
                th.Property("code", th.StringType),
                th.Property("email", th.StringType),
                th.Property("first_name", th.StringType),
                th.Property("last_name", th.StringType),
                th.Property("company", th.StringType),
                th.Property("parent_account_id", th.StringType),
                th.Property("bill_to", th.StringType),
                th.Property("dunning_campaign_id", th.StringType),
            ),
        ),
        th.Property(
            "plan",
            th.ObjectType(
                th.Property("id", th.StringType),
                th.Property("object", th.StringType),
                th.Property("code", th.StringType),
                th.Property("name", th.StringType),
            ),
        ),
        th.Property("state", th.StringType),
        th.Property("shipping", th.CustomType({"type": ["object", "string"]})),
        th.Property("coupon_redemptions", th.CustomType({"type": ["array", "string"]})),
        th.Property("pending_change", th.CustomType({"type": ["object", "string"]})),
        th.Property("current_period_started_at", th.DateTimeType),
        th.Property("current_period_ends_at", th.DateTimeType),
        th.Property("current_term_started_at", th.DateTimeType),
        th.Property("current_term_ends_at", th.DateTimeType),
        th.Property("trial_started_at", th.DateTimeType),
        th.Property("trial_ends_at", th.DateTimeType),
        th.Property("remaining_billing_cycles", th.IntegerType),
        th.Property("total_billing_cycles", th.IntegerType),
        th.Property("renewal_billing_cycles", th.IntegerType),
        th.Property("auto_renew", th.BooleanType),
        th.Property("ramp_intervals", th.CustomType({"type": ["array", "string"]})),
        th.Property("paused_at", th.DateTimeType),
        th.Property("remaining_pause_cycles", th.IntegerType),
        th.Property("currency", th.StringType),
        th.Property("revenue_schedule_type", th.StringType),
        th.Property("revenue_schedule_type", th.StringType),
        th.Property("unit_amount", th.NumberType),
        th.Property("tax_inclusive", th.BooleanType),
        th.Property("quantity", th.IntegerType),
        th.Property("add_ons", th.CustomType({"type": ["array", "string"]})),
        th.Property("add_ons_total", th.NumberType),
        th.Property("subtotal", th.NumberType),
        th.Property("tax", th.NumberType),
        th.Property("tax_info", th.CustomType({"type": ["object", "string"]})),
        th.Property("total", th.NumberType),
        th.Property("collection_method", th.StringType),
        th.Property("po_number", th.StringType),
        th.Property("net_terms", th.NumberType),
        th.Property("terms_and_conditions", th.StringType),
        th.Property("customer_notes", th.StringType),
        th.Property("expiration_reason", th.StringType),
        th.Property("custom_fields", th.CustomType({"type": ["array", "string"]})),
        th.Property("created_at", th.DateTimeType),
        th.Property("updated_at", th.DateTimeType),
        th.Property("activated_at", th.DateTimeType),
        th.Property("canceled_at", th.DateTimeType),
        th.Property("expires_at", th.DateTimeType),
        th.Property("bank_account_authorized_at", th.DateTimeType),
        th.Property("gateway_code", th.StringType),
        th.Property("billing_info_id", th.StringType),
        th.Property("active_invoice_id", th.StringType),
        th.Property("started_with_gift", th.BooleanType),
        th.Property("converted_at", th.DateTimeType),
    ).to_dict()


class TransactionsStream(RecurlyStream):
    name = "transactions"
    path = "/transactions"
    primary_keys = ["id"]
    replication_key = "updated_at"

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("object", th.StringType),
        th.Property("uuid", th.StringType),
        th.Property("original_transaction_id", th.StringType),
        th.Property(
            "account",
            th.ObjectType(
                th.Property("id", th.StringType),
                th.Property("object", th.StringType),
                th.Property("code", th.StringType),
                th.Property("email", th.StringType),
                th.Property("first_name", th.StringType),
                th.Property("last_name", th.StringType),
                th.Property("company", th.StringType),
                th.Property("parent_account_id", th.StringType),
                th.Property("bill_to", th.StringType),
                th.Property("dunning_campaign_id", th.StringType),
            ),
        ),
        th.Property(
            "invoice",
            th.ObjectType(
                th.Property("id", th.StringType),
                th.Property("object", th.StringType),
                th.Property("number", th.StringType),
                th.Property("type", th.StringType),
                th.Property("state", th.StringType),
            ),
        ),
        th.Property("voided_by_invoice", th.CustomType({"type": ["object", "string"]})),
        th.Property("subscription_ids", th.CustomType({"type": ["array", "string"]})),
        th.Property("type", th.StringType),
        th.Property("origin", th.StringType),
        th.Property("currency", th.StringType),
        th.Property("amount", th.NumberType),
        th.Property("status", th.StringType),
        th.Property("success", th.BooleanType),
        th.Property("backup_payment_method_used", th.BooleanType),
        th.Property("refunded", th.BooleanType),
        th.Property("billing_address", th.CustomType({"type": ["object", "string"]})),
        th.Property("collection_method", th.StringType),
        th.Property("payment_method", th.CustomType({"type": ["object", "string"]})),
        th.Property("ip_address_v4", th.StringType),
        th.Property("ip_address_country", th.StringType),
        th.Property("status_code", th.StringType),
        th.Property("status_message", th.StringType),
        th.Property("customer_message", th.StringType),
        th.Property("customer_message_locale", th.StringType),
        th.Property("payment_gateway", th.CustomType({"type": ["object", "string"]})),
        th.Property("gateway_message", th.StringType),
        th.Property("gateway_reference", th.StringType),
        th.Property("gateway_approval_code", th.StringType),
        th.Property("gateway_response_code", th.StringType),
        th.Property("gateway_response_time", th.NumberType),
        th.Property(
            "gateway_response_values", th.CustomType({"type": ["object", "string"]})
        ),
        th.Property("cvv_check", th.StringType),
        th.Property("avs_check", th.StringType),
        th.Property("created_at", th.DateTimeType),
        th.Property("updated_at", th.DateTimeType),
        th.Property("voided_at", th.DateTimeType),
        th.Property("collected_at", th.DateTimeType),
    ).to_dict()
