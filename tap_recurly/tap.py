"""Recurly tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th  # JSON schema typing helpers

# TODO: Import your custom stream types here:
from tap_recurly.streams import (
    RecurlyStream,
    AccountsStream,
    InvoicesStream,
    SubscriptionsStream,
    TransactionsStream,
)

STREAM_TYPES = [
    AccountsStream,
    InvoicesStream,
    SubscriptionsStream,
    TransactionsStream,
]


class TapRecurly(Tap):
    """Recurly tap class."""

    name = "tap-recurly"

    config_jsonschema = th.PropertiesList(
        th.Property("api_key", th.StringType, required=True),
        th.Property(
            "region",
            th.StringType,
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapRecurly.cli()
